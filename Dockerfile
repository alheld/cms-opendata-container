FROM cmsopendata/cmssw_7_6_7-slc6_amd64_gcc493

SHELL [ "/bin/zsh", "-c" ]

USER root

RUN usermod -u 7355 cmsusr && groupmod -g 10092 cmsusr

#RUN find / -uid 1000 -print0 | xargs -0 chown cmsuser
#RUN find / -gid 1000 -print0 | xargs -0 chgrp cmsuser

USER cmsusr 
RUN echo $0 && \
    source /opt/cms/entrypoint.sh && \
    cd /home/cmsusr/CMSSW_7_6_7/src && \
    git clone https://github.com/alexander-held/PhysObjectExtractorTool.git && \
    cd PhysObjectExtractorTool && \
    git checkout fix/2015-btagging && \
    scram b

# doing this first changes permissions for target folder for some reason
COPY poet_cfg.py /home/cmsusr/CMSSW_7_6_7/src/poet_cfg.py
